# Circuitpython Sourcecode

This is the circuitpython sourcecode for project canarchy.
It is provided on the board in plaintext readable files as well.

You can find the related homepage with documentation here: https://canarchy.io



## Description
Canarchy is an Open Hardware / Open Source CANbus exploration tool.
It can be used in a wide range of applications.

Features include:
 - 3 User programmable Touch inputs
 - An OLED display
 - Up to 72V DC input to power it directly from your target
 - Hall sensor to activate something via magnet
 - Slcand support for native Linux integration 

Custom behaviour is added in the app.py file.
In there are a number of callbacks that canarchy will call when an event occurs.
It's pretty self explaining.



## Support
Need help?
Got ideas for new features?
Please open an issue here.



## Contributing
Contributions are welcome.
Please clone this repo, do your changes and submit a pull request describing what you are changed / added or fixed.


## Authors and acknowledgment
This project is currently a single person venture by overflo, but contributors are welcome!

## License
GPL-2.0-only  See LICENSE.txt file

## Project status
Hyperactive
