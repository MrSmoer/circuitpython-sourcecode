####
# Late 2022
# Author overflo
# Part of CANarchy
# Filename: canarchy.py
# Purpose: the actual canarchy project
# License Details found @ /LICENSE file in this repository


import sys


sys.path.append("/sd/canarchy")
sys.path.append("/sd/canarchy/lib")
sys.path.append("/sd/canarchy/modules")

sys.path.append("/canarchy/lib")

import os
import board
import time
import digitalio
import math
import asyncio


class CANarchy(object):
    VERSION = "2.5"  # 1.4.24

    # BUS_EN pin with JFET
    bus_en = None

    # touch inputs on the bottom from canarchy/lib
    buttons = None

    # hall sensor from canarchy/lib
    hall = None

    # CANbus object from canarchy/lib
    can = None

    datalogger = None

    # slcan from canarchy/lib
    slcan = None

    # shows menu things on serial connect from canarchy/lib
    serialcom = None

    # display object from canarchy/lib
    display = None

    # wifi object might not be initalized
    wifi_control = None

    # the user defined app.py form / with all the callbacks
    app = None

    # the blue led on the ESP32-MINI-S2
    onboardled = None

    # SINGLETON for canarchy object -> look at __new__
    _instance = None

    def __new__(self):
        if self._instance is None:
            # TODO  mount SD, if there is a canarchy directory load everything from there.
            # if /sd/ is found id has to extend the sys.path.append("/sd/")

            print("Creating the one CANarchy singleton object")
            self._instance = super(CANarchy, self).__new__(self)

            # onboardled
            self.onboardled = digitalio.DigitalInOut(board.LED)
            self.onboardled.direction = digitalio.Direction.OUTPUT

            self.blinkled(self, 2)

            # BUS_EN
            self.bus_en = digitalio.DigitalInOut(board.IO4)
            self.bus_en.direction = digitalio.Direction.OUTPUT

            from .modules import display

            self.display = display.Display(self)

            # BUTTONS
            from .modules import buttons

            b = buttons.Buttons()
            b.addButton(board.IO8, "button1")
            b.addButton(board.IO10, "button2")
            b.addButton(board.IO13, "button3")
            self.buttons = b

            # HALL sensor
            from .modules import hall

            self.hall = hall.Hall()

            # Datalogger things
            from .modules import datalogger

            self.datalogger = datalogger.DataLogger(self)
            self.datalogger.createNewLogfile()

            # CANbus
            from .modules import can

            self.can = can.CANbus(board.IO3, board.IO2, 250000, self)
            self.can.addLogger(self.datalogger)

            # slcand support
            from .modules import slcan

            self.slcan = slcan.slcan(self)

            from .modules import serialcom

            self.serialcom = serialcom.serialcom(self)

            import app

            self.app = app
            self.app.app_init()

        return self._instance

    # do all things canarchy object itself does
    def work(self):
        try:
            self.app.periodic_callback()
        except Exception as e:
            print("ERROR in periodic_callback() ", e)
        # cleanup the status indicators on the display

    # the main loop called over and over again
    async def loop(self):
        while True:
            # print("doing all the canarchy things")
            self.work()
            self.buttons.work()
            self.hall.work()
            self.can.work()
            self.slcan.work()
            self.serialcom.work()
            self.display.work()
            if self.app.canarchy.wifi_control:
                self.app.canarchy.wifi_control.work()

            # await asyncio.sleep_ms(200)

    def start(self):
        loop = asyncio.get_event_loop()
        loop.create_task(self.loop())
        loop.run_forever()

    #                                               <ID>#<payload>
    # canarchy.send() takes input in the form of 12345678#1234567812345678
    def send(self, packet):
        self.can.send_block(packet)
        # TODO return something if can works or dont work

    # toggle the pullup pin on BUS_EN
    def toggle_bus_en(self):
        if self.bus_en.value:
            self.bus_en.value = False
            print("bus_en DISABLED")
        else:
            self.bus_en.value = True
            print("bus_en ENABLED")

    # blinks the onboard led X times
    def blinkled(self, times):
        # print("blinking ",times, " times")
        for x in range(times):
            self.onboardled.value = 1
            time.sleep(0.2)
            self.onboardled.value = 0
            time.sleep(0.1)

    # TODO implement this
    def replaylog(self, num):
        print("Replying: ", self.datalogger.logfiles[num][0])

    def setup_wifi(self, ssid, password):
        from .modules.wlan import WiFiControl

        self.wifi_control = WiFiControl(self, ssid, password)
        self.wifi_control.setup_wifi_ap()
        self.wifi_control.start_http_server()

        # TODO: this should happen inside the wifi class as soon as a client is connected
        self.display.set_active_flag(4, 1)
        self.display.add_line("---------------------")
        self.display.add_line("WiFi enabled")
        self.display.add_line(ssid + " / " + password)
        self.display.add_line("http://192.168.4.1")
        self.display.show_lines()

        print("wifi_setup(essid=" + ssid + ", password=" + password + ")")

    def set_can_bitrate(self, bitrate):
        print("canarchy.set_can_bitrate() called")
        try:
            self.can.set_bitrate(bitrate)
        except Exception as e:
            print("Error in set_can_bitrate()")

    # just testing things
    def hello(self):
        print("HELLO!")
