# Example app.py
# Needs canarchy.io Firmware 2.5


from canarchy import canarchy as c

canarchy = c.CANarchy()


def app_init():
    # start an accesspoint with essid and password.
    # webinterfaces are available on http://192.168.4.1
    essid = "CANarchy"
    wifi_password = "12345678"
    canarchy.setup_wifi(essid, wifi_password)

    canarchy.set_can_bitrate(250000)


def on():
    canarchy.toggle_bus_en()
    roll_on()
    canarchy.display.add_line("Turn ON")
    canarchy.display.show_lines()


def off():
    global rollin
    rollin = False
    print("Shutdown")
    canarchy.send("2342#1337")
    canarchy.toggle_bus_en()
    canarchy.display.add_line("Shutdown")
    canarchy.display.show_lines()


def eject():
    canarchy.toggle_bus_en()
    for i in range(3):
        canarchy.send("4223#ABCD")

    canarchy.display.add_line("Eject")
    canarchy.display.show_lines()


rollin = False


def roll_on():
    global rollin
    rollin = True

    canarchy.send("0815#ACAB")


# Button handlers
def button_callback_onTouch(label):
    if label == "button2":
        print("Button 2")
        eject()

    if label == "button3":
        print("Button 3")
        on()

    if label == "button1":
        print("Button 1")
        off()


# called as often as possible
last_callback = 0


import time


def periodic_callback():
    global last_callback
    now = time.monotonic()
    if last_callback == 0:
        last_callback = now

    # do something once per second
    if now - last_callback > 1:
        last_callback = now
        if rollin:
            roll_on()


def hall_callback_onChange(magnet_detected):
    print("hall_callback_onChange: ", magnet_detected)
    if not magnet_detected:
        if not rollin:
            on()
        else:
            off()


# called on CAN receive or send, implement logging here or something else.
def can_callback_onReceive(message):
    # print("can_callback_onReceive")
    data = message.data
    id_str = str(hex(message.id))[2:]
    # packet = "%s#%s" % (f'{int(id_str):08}',hexlify(message.data).decode("utf-8").upper())
    # print("CAN < " + packet)
    # can.dl.log(packet)


def can_callback_onSend(message):
    # print("can_callback_onSend")
    data = message.data
    id_str = str(hex(message.id))[2:]
    # print("CAN > %s#%s"%(f'{id_str:0>8}',hexlify(message.data).decode().upper()))
